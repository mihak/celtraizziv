var Game = new function() {                                                                  
  var boards = [];
  var stopLoop = false;
  var idd = null;
  var lastGameLoopFrame = null;

  // Game Initialization
  this.initialize = function(canvasElementId,sprite_data,callback) {
    this.canvas = document.getElementById(canvasElementId)
    this.width = this.canvas.width;
    this.height= this.canvas.height;
    this.offset1 = this.canvas.offsetLeft;

    this.ctx = this.canvas.getContext && this.canvas.getContext('2d');
    if(!this.ctx) { return alert("Please upgrade your browser to play"); }

	this.canvasMultiplier = 1;
    this.playerOffset = 10;
    
    this.isMobile = false;
    this.setupMobile();
    
    this.setupInput();
   
    if (this.isMobile){
    	//this.setBoard(2,new TouchControls());
    }
	
    //this.loop();
	this.animationLoop;
	(function() {
		var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
				  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
		window.requestAnimationFrame = requestAnimationFrame;
			})();
	(function() {
		var cancelAnimationFrame = window.cancelAnimationFrame || window.cancelRequestAnimationFrame ||
				  window.cancelRequestAnimationFrame || window.cancelRequestAnimationFrame;
		window.cancelAnimationFrame = cancelAnimationFrame;
			})();
    if (window.requestAnimationFrame){
    	window.requestAnimationFrame(loopRAF);
    	//loopRAF();
    	this.animationLoop = "RAF";
    } else {
    	this.loop();
    	this.animationLoop = "Timeout";
    }
    
	var p = navigator.platform;
	
	if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
	    iOS = true;
	}
    
    SpriteSheet.load(sprite_data,callback);
 
  };
  	
  // Handle Input	 - first tests runs on PC on Google Chrome
  var KEY_CODES = { 37:'left', 39:'right', 32 :'fire' };
  this.keys = {};

  this.setupInput = function() {
    window.addEventListener('keydown',function(e) {
      if(KEY_CODES[event.keyCode]) {
       Game.keys[KEY_CODES[event.keyCode]] = true;
       e.preventDefault();
      }
    },false);

    window.addEventListener('keyup',function(e) {
      if(KEY_CODES[event.keyCode]) {
       Game.keys[KEY_CODES[event.keyCode]] = false; 
       e.preventDefault();
      }
    },false);
  }

  // Game Loop	- Timeout support for older devices
  this.loop = function() { 
    var dt = 30 / 1000;
	Game.ctx.clearRect(0, 0, Game.width, Game.height);
    for(var i=0,len = boards.length;i<len;i++) {
      if(boards[i]) { 
        boards[i].step(dt);
        boards[i].draw(Game.ctx);
      }
    }

    setTimeout(Game.loop,30);
  };
  
  	// Game loop - requestAnimationFrame
 	 
	loopRAF = function(timestamp) { 
		var dt = (timestamp - lastGameLoopFrame) / 1000;
		//console.log("ID: "+idd+" - DT: "+dt);
		lastGameLoopFrame = timestamp;
		Game.ctx.clearRect(0, 0, Game.width, Game.height);
	    for(var i=0,len = boards.length;i<len;i++) {
	      if(boards[i]) { 
	        boards[i].step(dt);
	        boards[i].draw(Game.ctx);
	      }
	    }
	    if (!stopLoop){
	    	window.requestAnimationFrame(loopRAF);
	    }
  	};
  	
  	this.stopLoopFunc = function(){ 
	  		stopLoop = true;
	  		if (idd){
	  			window.cancelAnimationFrame(idd);
	  		}
  		}
  	this.startLoopFunc = function(){ 
  		stopLoop = false;
  		window.requestAnimationFrame(loopRAF);
  		//idd = window.requestAnimationFrame(loopRAF); 
  		}
  		
  	this.narisiNekaj = function(){
  		
  		Game.ctx.fillStyle = "#888888";
     	Game.ctx.textAlign = "left";
     	Game.ctx.font = "bold 15px bangers";
	    
	     Game.ctx.fillText("RIŠEMO NEKAJ!!!",5,20);
  	};
  
  // Change an active game board
  this.setBoard = function(num,board) { boards[num] = board; };
  this.clearBoard = function() {boards = []};


  this.setupMobile = function() {
    var container = document.getElementById("container"),
        hasTouch =  !!('ontouchstart' in window),
        w = window.innerWidth, h = window.innerHeight;

	//this.isMobile = ('ontouchstart' in window);
	
    if(hasTouch) { mobile = true; this.isMobile = true;}

    if(screen.width >= 1280 || !hasTouch) { return false; }

    if(w > h) {
      alert("Please rotate the device and then click OK");
      w = window.innerWidth; h = window.innerHeight;
    }

    container.style.height = h*2 + "px";
    window.scrollTo(0,1);

    h = window.innerHeight + 2;
    container.style.height = h + "px";
    container.style.width = w + "px";
    container.style.padding = 0;

    if(h >= this.canvas.height * 1.15 || w >= this.canvas.height * 1.15) {
      var f = w/this.canvas.width;
      this.canvasMultiplier = f;
      this.canvas.width = w / f;
      this.canvas.height = h / f;
      this.canvas.style.width = w + "px";
      this.canvas.style.height = h + "px";
    } else {
      this.canvas.width = w;
      this.canvas.height = h;
    }

    this.canvas.style.position='absolute';
    this.canvas.style.left="0px";
    this.canvas.style.top="0px";

  };
};


var SpriteSheet = new function() {
  this.map = { }; 

  this.load = function(spriteData,callback) { 
    this.map = spriteData;
    this.image = new Image();
    this.image.onload = callback;
    // let's borrow some image in the beginning
    this.image.src = 'images/mysprites.png';
  };

  this.draw = function(ctx,sprite,x,y,frame) {
    var s = this.map[sprite];
    if(!frame) frame = 0;
    ctx.drawImage(this.image,
                     s.sx + frame * s.w, 
                     s.sy, 
                     s.w, s.h, 
                     Math.floor(x), Math.floor(y),
                     s.w, s.h);
  };
};

var GameBoard = function() {
  var board = this;

  // The current list of objects
  this.objects = [];

  // Add a new object to the object list
  this.add = function(obj) { 
    obj.board=this; 
    this.objects.push(obj); 
    return obj; 
  };

  // Mark an object for removal
  this.remove = function(obj) { 
    this.removed.push(obj); 
  };
  
  // Check if object is already marked for removal - used for multiple missiles
  this.alreadyRemoved = function(obj){
  	for(var i=0,len=this.removed.length;i<len;i++) {
  		if (obj==this.removed[i]){
  			return true;
  		}	
  	}
  	return false;
  }

  // Reset the list of removed objects
  this.resetRemoved = function() { this.removed = []; }

  // Removed an objects marked for removal from the list
  this.finalizeRemoved = function() {
    for(var i=0,len=this.removed.length;i<len;i++) {
      var idx = this.objects.indexOf(this.removed[i]);
      if(idx != -1) this.objects.splice(idx,1);
    }
  }


  // Call the same method on all current objects 
  this.iterate = function(funcName) {
     var args = Array.prototype.slice.call(arguments,1);
     for(var i=0,len=this.objects.length;i<len;i++) {
       var obj = this.objects[i];
       obj[funcName].apply(obj,args)
     }
  };

  // Find the first object for which func is true
  this.detect = function(func) {
    for(var i = 0,val=null, len=this.objects.length; i < len; i++) {
      if(func.call(this.objects[i])) return this.objects[i];
    }
    return false;
  };

  // Call step on all objects and them delete
  // any object that have been marked for removal
  this.step = function(dt) { 
    this.resetRemoved();
    this.iterate('step',dt);
    this.finalizeRemoved();
  };

  // Draw all the objects
  this.draw= function(ctx) {
    this.iterate('draw',ctx);
  };

  // very simple cooision detection, so we can keep FPS as high as possible
  // Check for a collision between the 
  // bounding rects of two objects
  this.overlap = function(o1,o2) {
    return !((o1.y+o1.h-1<o2.y) || (o1.y>o2.y+o2.h-1) ||
             (o1.x+o1.w-1<o2.x) || (o1.x>o2.x+o2.w-1));
  };

  // Find the first object that collides with obj
  // match against an optional type
  this.collide = function(obj,type) {
    return this.detect(function() {
      if(obj != this) {
       var col = (!type || this.type & type) && board.overlap(obj,this)
       return col ? this : false;
      }
    });
  };
};

var TouchControls = function() {

  var gutterWidth = 10;
  var unitWidth = Game.width/5;
  var blockWidth = unitWidth-gutterWidth;

  this.drawSquare = function(ctx,x,y,txt,on) {
    ctx.globalAlpha = on ? 0.6 : 1;
    ctx.fillStyle =  "#555";  
    ctx.fillRect(x,y,blockWidth,blockWidth);
    ctx.fillStyle =  "#333";  
    ctx.fillRect(x+5,y+5,blockWidth-10,blockWidth-10);

    ctx.fillStyle = "#FFF";
    ctx.textAlign = "center";
	ctx.textBaseline = 'middle';
    ctx.globalAlpha = 1.0;
    ctx.font = "bold " + (3*unitWidth/4) + "px arial";

    ctx.fillText(txt, 
                 x+blockWidth/2,
                 y+blockWidth/2);
  };

  this.draw = function(ctx) {
    ctx.save();

    var yLoc = Game.height - unitWidth;
    this.drawSquare(ctx,gutterWidth,yLoc,"<", Game.keys['left']);
    this.drawSquare(ctx,unitWidth + gutterWidth,yLoc,">", Game.keys['right']);
    this.drawSquare(ctx,4*unitWidth,yLoc,"x",Game.keys['fire']);

    ctx.restore();
  };

  this.step = function(dt) { };

  this.trackTouch = function(e) {
    var touch, x;

    e.preventDefault();
    Game.keys['left'] = false;
    Game.keys['right'] = false;
    for(var i=0;i<e.targetTouches.length;i++) {
      touch = e.targetTouches[i];
      x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
      if(x < unitWidth) {
        Game.keys['left'] = true;
      } 
      if(x > unitWidth && x < 2*unitWidth) {
        Game.keys['right'] = true;
      } 
    }

    if(e.type == 'touchstart' || e.type == 'touchend') {
      for(i=0;i<e.changedTouches.length;i++) {
        touch = e.changedTouches[i];
        x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
        if(x > 4 * unitWidth) {
          Game.keys['fire'] = (e.type == 'touchstart');
        }
      }
    }
  };

  Game.canvas.addEventListener('touchstart',this.trackTouch,true);
  Game.canvas.addEventListener('touchmove',this.trackTouch,true);
  Game.canvas.addEventListener('touchend',this.trackTouch,true);

  Game.playerOffset = unitWidth + 5;
};



