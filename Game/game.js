var sprites = {
 ship: { sx: 110, sy: 0, w: 22, h: 40, frames: 1 },
 missile: { sx:110, sy: 40, w: 7, h: 20, frames: 1 },
 ballXL: { sx: 50, sy: 30, w: 60, h: 60, frames: 1 },
 ballL: { sx: 0, sy: 0, w: 50, h: 50, frames: 1 },
 ballM: { sx: 0, sy: 50, w: 40, h: 40, frames: 1 },
 ballS: { sx: 50, sy: 0, w: 30, h: 30, frames: 1 },
 ballXS: { sx: 80, sy: 0, w: 20, h: 20, frames: 1 },
 star: { sx: 132, sy: 30, w: 30, h: 30, frames: 1 },
 missile1: { sx: 132, sy: 0, w: 30, h: 30, frames: 1 },
 missile2: { sx: 162, sy: 0, w: 30, h: 30, frames: 1 },
 missile3: { sx: 192, sy: 0, w: 30, h: 30, frames: 1 },
 gravityH: { sx: 162, sy: 30, w: 30, h: 30, frames: 1 },
 gravityL: { sx: 192, sy: 30, w: 30, h: 30, frames: 1 },
 missileR1: { sx: 132, sy: 60, w: 30, h: 30, frames: 1 },
 missileR2: { sx: 162, sy: 60, w: 30, h: 30, frames: 1 },
 missileR3: { sx: 192, sy: 60, w: 30, h: 30, frames: 1 },
 ballXL_S: { sx: 222, sy: 0, w: 20, h: 20, frames: 1 },
 ballL_S: { sx: 222, sy: 20, w: 20, h: 20, frames: 1 },
 ballM_S: { sx: 222, sy: 40, w: 20, h: 20, frames: 1 }

};

var OBJECT_PLAYER = 1,
    OBJECT_PLAYER_PROJECTILE = 2,
    OBJECT_BALL = 4;
    OBJECT_SPECIALS = 8;

// we need to handle iOS differently because of the status bar in full screen mode
iOS = false; 
    
// simple points counter
var hits = 0;

//	Level mode ("level") or Time attack ("time")
var gameMode = "level"
var ballsOnScreen = 0;
var currentLevelIndex = 0;
var currentLevel = [];
var showNextBallTime = [];
var levelModeSpecialBalls = [];
var levelModeSpecialBallsWhenToShow = [];

// Time Attack specifics
var timeAttackLevel = ["ballL", "ballS"];
var timeAttackNewBallInterval = 3;
						
// which special balls are available in Time Attack mode		
var specialBallsAvailable = ["missile1","missile2","missile3","missileR1","missileR2","missileR3","gravityH","gravityL","missile1","missile2","missileR1","missileR2","gravityH","gravityL"];
var timeAttackSpecialBalls;
// Time Attack duration
var timeAttackLimit = 60;


// this needs to be reset every new game or level 
var noOfMissiles = 1;
var ballSpeed = 0;    
var ballGravity = null;  

// with debug mode enabled, you can get extra info of the game (eg. objects on screen, FPS)
var debugMode = false;


/*
 *	START GAME 
 */
var startGame = function() {
	Game.setBoard(1, new MainMenu());
	DrawBackgroundBalls();
}

// when level is cleard successfully 
var nextLevel = function() {
	Game.clearBoard();
	Game.setBoard(1, new LevelMenu(true));
	DrawBackgroundBalls();
}

// when lavel failed
var levelFailed = function() {
	ballsOnScreen = 0;
	Game.clearBoard();
	Game.setBoard(1, new LevelMenu(false));
	DrawBackgroundBalls();
}

// when time is out in Time Attack
var timeAttackEnded = function() {
	ballsOnScreen = 0;
	Game.clearBoard();
	Game.setBoard(1, new TimeAttackMenu());
	DrawBackgroundBalls();
}

/*
 * 	Magic of each level starts here
 * 
 */
var playGame = function() {
	// reset start values
	noOfMissiles = 1;
	ballSpeed = 0;    
	ballGravity = null;  
		
	hits = 0;

	// Level mode
	if (gameMode == "level"){
		
		// prepare level data
		currentLevel = levels[currentLevelIndex][0];
		showNextBallTime = levels[currentLevelIndex][1].slice(0);
		levelModeSpecialBalls = levels[currentLevelIndex][2].slice(0);
		
		var powerOf = levelModeSpecialBalls.shift();
		var maxNumber = (Math.pow(2,powerOf)-1)*(showNextBallTime.length+1);
		
		// calculate when scecial balls should appear
		for(var i=0,len = levelModeSpecialBalls.length;i<len;i++) {
			levelModeSpecialBallsWhenToShow[i] = Math.round(Math.random()*maxNumber);
		}
		levelModeSpecialBallsWhenToShow.sort(function(a,b){return a-b});
		
		// shuffle special balls so they don't appear in the same order every time 
		shuffle(levelModeSpecialBalls);
		console.log(levelModeSpecialBalls);
		console.log(levelModeSpecialBallsWhenToShow);
		
		var board = new GameBoard();
		// add first ball and a ship
		board.add(new PlayerShip());
		board.add(new Balls(Game.width/2-25,0,90,currentLevel[0]));
		Game.setBoard(1, board);
	} 
	// Time Attack
	else {
		timeAttackSpecialBalls = specialBallsAvailable.slice(0);
		shuffle(timeAttackSpecialBalls);
		
		var board = new GameBoard();
		// add first ball and a ship
		board.add(new PlayerShip());
		board.add(new Balls(Game.width/2-25,0,90,timeAttackLevel[0]));
		Game.setBoard(1, board);
	}
}

/*
 * whait until everything is loaded, then start this beautiful game :)
 */
window.addEventListener("load", function() {
  Game.initialize("game",sprites,startGame);

});


var Balls = function(sX,sY,angle,ballType) { 
   this.x = sX;
   this.y = sY;
   this.predznakX = 1;
   this.predznakY = 1;
   this.ballType = ballType;
   
   this.angle = (angle*Math.PI)/180;
   
   console.log(currentLevel[1]);
   if (!ballGravity){
   		
   		/* TIMEOUT - DEPRICATED */
	   if (Game.animationLoop == "Timeout"){
	   		ballSpeed = 2.5;
			ballGravity = 0.03;
	   } else {
	   		if (currentLevel[1] == "ballXS"){
	   			ballSpeed = 0.5;
				ballGravity = 0.005;
	   		} else {
				ballSpeed = 0.7;
				ballGravity = 0.01;
			}
	   }
   }
   
   console.log("Speed: "+ballSpeed);
   console.log("Gravity: "+ballGravity);
	      
   this.velocityX = ballSpeed * Math.cos(this.angle);
   this.velocityY = ballSpeed * Math.sin(this.angle);
   
   this.w =  SpriteSheet.map[this.ballType].w;
   this.h =  SpriteSheet.map[this.ballType].h;
   
   ballsOnScreen += 1;
}

   Balls.prototype.step = function(dt) {
		this.velocityY += ballGravity;
		this.x += this.velocityX*this.predznakX;
		this.y += this.velocityY; 
		
		// in level mode, we set borders, so the balls can bounce from it
		if (gameMode == "level"){
			if (this.x > Game.width-this.w || this.x <= 0){
				this.predznakX *= -1;
			} 
			
			if (this.y < 0){
				this.y = 0;
			}
			
			if (this.y > Game.height-this.h){
				//if ball falls of the screen, level fails 
				levelFailed();
			}
		} else {
			if (this.x <= 0-this.w || this.x > Game.width || this.y > Game.height-this.h){
				this.board.remove(this);
				ballsOnScreen-=1;
			}
		}
			
   }

   Balls.prototype.draw = function(ctx) {
     SpriteSheet.draw(ctx,this.ballType,this.x,this.y);
   }
   
   Balls.prototype.type = OBJECT_BALL;



var PlayerShip = function() { 
   this.w =  SpriteSheet.map['ship'].w;
   this.h =  SpriteSheet.map['ship'].h;
   this.x = Game.width/2 - this.w / 2;
   this.y = Game.height - Game.playerOffset - this.h;
   this.vx = 0;
   
   // missile reloading time - one cannot fire another missile until certain amount of time elapses
   this.reloadTime = 0.4;
   this.reload = this.reloadTime;
   
   // ship speed (left/right)
   this.maxVelocity = 320;
   
   // internal timer for each game
   this.timeEllapsed = 0;
   
   
   this.timeAttackNewBallTime = timeAttackNewBallInterval;
   this.timeAttackSpecialBallTime = Math.random()*4+3;

   var fps = 0, now, lastUpdate = (new Date)*1 - 1;

   this.step = function(dt) {
   	 // lets handle moving the ship left and right
     if(Game.keys['left']) { this.vx = -this.maxVelocity; }
     else if(Game.keys['right']) { this.vx = this.maxVelocity; }
     else { this.vx = 0; }  

     this.x += this.vx * dt;

     if(this.x < 0) { this.x = 0; }
     else if(this.x > Game.width - this.w) { 
       this.x = Game.width - this.w 
     }
     
     // handle collision with special objects
     var collision = this.board.collide(this,OBJECT_SPECIALS);
     if (collision){
     	this.board.remove(collision);
     	handleSpecials(this, collision);
     }
     
     
	this.reload -= dt;
	if(Game.keys['fire'] && this.reload < 0) {
	   this.reload = this.reloadTime;
	   if (noOfMissiles == 3){
	   	this.board.add(new PlayerMissile(this.x,this.y+this.h/2));
	   	this.board.add(new PlayerMissile(this.x+this.w/2-3,this.y));
	   	this.board.add(new PlayerMissile(this.x+this.w-7,this.y+this.h/2));
	   } else if (noOfMissiles == 2){
	   	this.board.add(new PlayerMissile(this.x,this.y+this.h/2));
	   	this.board.add(new PlayerMissile(this.x+this.w-7,this.y+this.h/2));
	   } else {
       	this.board.add(new PlayerMissile(this.x+this.w/2,this.y+this.h/2));
       }
     }        
     
     // Level mode
     if (gameMode == "level"){
		 	// when level over, start next or end game
		     if (ballsOnScreen <= 0 && showNextBallTime.length == 0){
		     	if (currentLevelIndex < levels.length -1 ) {
		     		currentLevelIndex +=1;
		     		nextLevel();
		 		} else {
		 			Game.setBoard(1,new GameEnded());
		 		}	
		     }
		     
		     // adding new balls on screen
		     this.timeEllapsed+=dt;
		     if (showNextBallTime.length > 0 && this.timeEllapsed > showNextBallTime[0]){
		     	showNextBallTime.shift();
		     	var bx = SpriteSheet.map[currentLevel[0]].w;
		     	var xx = Math.round(Math.random()*(Game.width-bx));
		     	this.board.add(new Balls(xx,0,90,currentLevel[0]));
		     }
		     
		     // adding special balls
		     if (levelModeSpecialBalls.length > 0 && levelModeSpecialBallsWhenToShow.length > 0 && levelModeSpecialBallsWhenToShow[0] == hits){
		     	this.board.add(new Specials(Math.random()*(Game.width-30),0,levelModeSpecialBalls[0]));
		     	levelModeSpecialBalls.shift();
		     	levelModeSpecialBallsWhenToShow.shift();
		     }
		} 
		// Time Attack
		else {
			this.timeEllapsed+=dt;
			if (this.timeEllapsed > timeAttackLimit){
				Game.clearBoard();
				timeAttackEnded();
			}
			else if (this.timeEllapsed > this.timeAttackNewBallTime){
				// add new ball on static interval (eg. 3 seconds)
				//this.timeAttackNewBallTime += timeAttackNewBallInterval;
				
				// add new ball dynamically (random between 1 and 4 seconds)
				var randomInterval = Math.random()*3+1.5;
				this.timeAttackNewBallTime += randomInterval;
				
				var bx = SpriteSheet.map[timeAttackLevel[0]].w;
		     	var xx = Math.round(Math.random()*(Game.width-bx));
		     	this.board.add(new Balls(xx,0,90,timeAttackLevel[0]));
			}
			
			// adding special balls
			if (timeAttackSpecialBalls.length > 0 && this.timeEllapsed > this.timeAttackSpecialBallTime){
				this.board.add(new Specials(Game.width/2,0,timeAttackSpecialBalls[0]));
				timeAttackSpecialBalls.shift();
				
				var randomInterval = Math.random()*5+6;
				console.log(randomInterval);
				this.timeAttackSpecialBallTime += randomInterval;
			}
			
		}
     
 
   }

   this.draw = function(ctx) {
     SpriteSheet.draw(ctx,'ship',this.x,this.y,0);
     
     ctx.fillStyle = "#FFFFFF";
     ctx.textAlign = "center";
     ctx.font = "bold 20px bangers";
     
     var offset = 0;
     // if iOS, set offset
     if (iOS) { offset = 20; }
     
     if(debugMode){
     	ctx.fillStyle = "#888888";
     	ctx.textAlign = "left";
     	ctx.font = "bold 15px bangers";
	     var objects = this.board.objects.length;
	     // show active objects - used for debug
	     ctx.fillText("Objects active: "+objects,5,20+offset);
	     // show no. of hits
	     ctx.fillText("Hits: "+hits,5,40+offset);
	     
	     //****************
		 // The higher this value, the less the FPS will be affected by quick changes
		 // Setting this to 1 will show you the FPS of the last sampled frame only
		 var fpsFilter = 1;
		 var thisFrameFPS = 1000 / ((noow=new Date) - lastUpdate);
		 fps += (thisFrameFPS - fps) / fpsFilter;
		 lastUpdate = noow;
		 
		 ctx.textAlign = "right";
		 ctx.fillText("FPS: "+Math.round(fps),Game.width-5,20+offset);
		 ctx.fillText(Game.animationLoop,Game.width-5,40+offset);
		 
		 if (gameMode == "level"){
	     	 ctx.textAlign = "left";
	     	 ctx.fillText("Level "+(currentLevelIndex+1),5,60+offset);	
	     	 ctx.textBaseline = 'top';
			 ctx.fillText(showNextBallTime.length+"x", Game.width-28,60+offset);
			 SpriteSheet.draw(ctx,currentLevel[0]+"_S",Game.width-25,60+offset,0);
     	} else {
     		var tl = timeAttackLimit - this.timeEllapsed;
     		ctx.textAlign = "left";
	     	ctx.fillText("Time: "+tl.toFixed(2),5,60+offset);
	     	
	     	if (tl <= 5){
		     	// last 3 seconds countdown
		     	ctx.save();
			    
				ctx.globalAlpha = tl - Math.floor(tl);
				ctx.fillStyle =  "#F00";    
				
				ctx.textAlign = "center";
				ctx.textBaseline = 'middle';
			    ctx.font = "bold 100px arial";
			    ctx.fillText(tl.toFixed(1), Game.width/2, Game.height/2);
			    
			    ctx.restore();
		    }
     	}
			
     } else {
     	ctx.save();
     	if (gameMode == "level"){
	     	ctx.textAlign = "left";
	     	ctx.fillText("Level "+(currentLevelIndex+1),5,20+offset);
	     	ctx.textAlign = "right";
			ctx.fillText(showNextBallTime.length+"x", Game.width-30,20+offset);
			SpriteSheet.draw(ctx,currentLevel[0]+"_S",Game.width-25,2+offset,0);	
     	} else {
     		var tl = timeAttackLimit - this.timeEllapsed;
     		ctx.textAlign = "left";
	     	ctx.fillText("Time: "+tl.toFixed(1),5,20+offset);
	     	ctx.textAlign = "right";
     		ctx.fillText("Hits: "+hits,Game.width -5,20+offset);
     		
     		var tl = timeAttackLimit - this.timeEllapsed;
	     	
	     	if (tl <= 5){
		     	// last 3 seconds countdown
		     	ctx.save();
			    
				ctx.globalAlpha = tl - Math.floor(tl);
				ctx.fillStyle =  "#F00";    
				
				ctx.textAlign = "center";
				ctx.textBaseline = 'middle';
			    ctx.font = "bold 100px arial";
			    ctx.fillText(tl.toFixed(1), Game.width/2, Game.height/2);
			    
			    ctx.restore();
		    }
     	}
     	ctx.restore();
     	
     }
     
   }
   this.type = OBJECT_PLAYER;
}

var PlayerMissile = function(x,y) {
  this.w = SpriteSheet.map['missile'].w;
  this.h = SpriteSheet.map['missile'].h;
  this.x = x - this.w/2; 
  // Use the passed in y as the bottom of the missile
  this.y = y - this.h; 
  
  //missile speed
  this.vy = -300;
};

PlayerMissile.prototype.step = function(dt)  {
  this.y += this.vy * dt;
  if(this.y < -this.h) { this.board.remove(this); }
  
  var collision = this.board.collide(this,OBJECT_BALL);
  if (collision){
  	
  	
  	//fix for double (triple) missiles - if first missile already hit the ball, do nothing with the second missile
  	if (noOfMissiles > 1){
	  	ar = this.board.alreadyRemoved(collision);
	  	if (ar) { return;}
    }
  	
  	// remove object that missile collided with
  	this.board.remove(collision);
  	// remove missile
  	this.board.remove(this); 
  	hits+=1;
  	ballsOnScreen-=1
  	
  	var wBall = collision.w;
  	// with factor we calculate where missile hits the ball
  	var factor = (this.x+this.w/2) - Math.round(collision.x+collision.w/2);
  	//return alert(factor);
  	var maxFactor = wBall/2;
  	if (factor < maxFactor*(-1)) { factor = maxFactor*(-1)}
  	if (factor > maxFactor) {factor = maxFactor}
  	
  	// add two more balls on the screen
  	// balls are always split with fixed angle between
  	var angleFixed = 60;
  	var angle1 = Math.round(angleFixed*(factor/maxFactor)*-1);
  	var bType = "ballL";
  	var smallerBallsLeft = true;
  	
  	// set type of next balls to addon screen
  	if (gameMode == "level"){
	  	// ball always splits into next smaller balls
	  	switch(collision.ballType){
	  		case "ballXL":
	  			bType = "ballL";
	  			if (currentLevel[1] == "ballXL"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballL":
	  			bType = "ballM";
	  			if (currentLevel[1] == "ballL"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
			case "ballM":
				bType = "ballS";
				if (currentLevel[1] == "ballM"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballS":
				bType = "ballXS";
				if (currentLevel[1] == "ballS"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballXS":
	  			bType = "ballL"
	  			smallerBallsLeft = false;
	  			break;
	  	}
	  	
	  	if (smallerBallsLeft){
	  		// check if x possition in not off screen
	  		var newBallX = this.x-wBall/2;
	  		if (newBallX < 0) { newBallX = 0; }
	  		this.board.add(new Balls(newBallX,this.y-wBall/2,270+angle1-30,bType));
	  		this.board.add(new Balls(newBallX,this.y-wBall/2,270+angle1+30,bType));
	  	}
   }else {
	  	switch(collision.ballType){
	  		case "ballXL":
	  			bType = "ballL";
	  			if (timeAttackLevel[1] == "ballXL"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballL":
	  			bType = "ballM";
	  			if (timeAttackLevel[1] == "ballL"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
			case "ballM":
				bType = "ballS";
				if (timeAttackLevel[1] == "ballM"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballS":
				bType = "ballXS";
				if (timeAttackLevel[1] == "ballS"){
	  				smallerBallsLeft = false;
	  			}
	  			break;
	  		case "ballXS":
	  			bType = "ballL"
	  			smallerBallsLeft = false;
	  			break;
	  	}
	  	
	  	if (smallerBallsLeft){
	  		this.board.add(new Balls(this.x-wBall/2,this.y-wBall/2,270+angle1-30,bType));
	  		this.board.add(new Balls(this.x-wBall/2,this.y-wBall/2,270+angle1+30,bType));
	  	}
   }
  	 	
  }
};

PlayerMissile.prototype.draw = function(ctx)  {
  SpriteSheet.draw(ctx,'missile',this.x,this.y);
};

PlayerMissile.prototype.type = OBJECT_PLAYER_PROJECTILE;
