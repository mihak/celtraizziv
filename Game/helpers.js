/**
 * suffle the array so the items are in random order
 * 
 * @param {Object} array - an array to be suffled
 */
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

/*
 * Draw this screen before each game (level) starts
 * Sorry, a little inappropriate naming :)
 */
var HomeScreen = function HomeScreen(callback){
	this.timeout = 0;
	this.waitTime = 3;
	
	this.step = function(dt) { 
		this.timeout+=dt;
		this.waitTime -= dt;
		if (this.timeout > 3){
			callback();
		}
	}
	
	this.draw = function(ctx) {
		
		ctx.fillStyle = "#FFFFFF";
	    ctx.textAlign = "center";
	
	    ctx.font = "bold 20px bangers";
	    if (gameMode == "level"){
	    	ctx.fillText("Level "+(currentLevelIndex+1)+" starts in "+Math.round(this.waitTime),Game.width/2,Game.height/2);	
	    } else {
	    	ctx.fillText("Time Attack starts in "+Math.round(this.waitTime),Game.width/2,Game.height/2);
	    }
	    
		
	   }
}

/*
 * Game over!
 */
var GameEnded = function GameEnded(){
	this.timeout = 0;
	this.waitTime = 3;
	
	this.step = function(dt) { 
		this.timeout+=dt;
		this.waitTime -= dt;
		if (this.timeout > 3){
			startGame();
		}
	}
	
	this.draw = function(ctx) {
		
		ctx.fillStyle = "#CCC";
	    ctx.textAlign = "center";
	
	    ctx.font = "bold 40px bangers";
	    ctx.fillText("Game over",Game.width/2,Game.height/2);
	   }
}
