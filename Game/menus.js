	var noOfButtons = 5;
	var marginLeftRight = 25;
	var marginTopBottom = 15;

// position - which button (from 1-5 - index runs from 0-4)
var drawMenuButton = function(ctx, position, msg){
	var buttonWidth = Game.width - marginLeftRight*2;
	var buttonHeight = Game.height/noOfButtons - 2*marginTopBottom;
	var buttonAreaHeight = Game.height/noOfButtons;

	ctx.save();
    
	// button rectange
	ctx.globalAlpha = 0.9;
	ctx.fillStyle =  "#555";    
	ctx.fillRect(marginLeftRight,buttonAreaHeight*position+marginTopBottom,buttonWidth,buttonHeight);
	ctx.fillStyle =  "#333";  
	ctx.fillRect(marginLeftRight+5,buttonAreaHeight*position+marginTopBottom+5,buttonWidth-10,buttonHeight-10);
	// button text
	ctx.textAlign = "center";
	ctx.textBaseline = 'middle';
	ctx.globalAlpha = 1.0;
    ctx.font = "bold 30px arial";
    ctx.fillStyle =  "#CCC";
    ctx.fillText(msg, Game.width/2, buttonAreaHeight*position + buttonAreaHeight/2);
    
    ctx.restore();
}

// draw text without rectange
var drawMenuLabel = function(ctx, position, msg){
	var buttonWidth = Game.width - marginLeftRight*2;
	var buttonHeight = Game.height/noOfButtons - 2*marginTopBottom;
	var buttonAreaHeight = Game.height/noOfButtons;

	ctx.save();
    
    ctx.textAlign = "center";
	ctx.textBaseline = 'middle';
    ctx.font = "bold 40px arial";
    ctx.fillStyle =  "#CCC";
    if (msg == "CLEARED")	{ctx.fillStyle =  "#0F0"; ctx.font = "bold 30px arial";};
    if (msg == "FAILED")	{ctx.fillStyle =  "#F00"; ctx.font = "bold 30px arial";};
    ctx.fillText(msg, Game.width/2, buttonAreaHeight*position + buttonAreaHeight/2);
    
    ctx.restore();
}

var MainMenu = function(){
	var buttonAreaHeight = Game.height/noOfButtons;
	currentLevelIndex = 0;
	hits = 0;
	
    this.step = function(dt) { }
	
	this.draw = function(ctx) {
		
		drawMenuLabel(ctx,0,"Split ball");
    	drawMenuButton(ctx,1,"Marathon");
    	drawMenuButton(ctx,2,"Time Attack");
    	drawMenuButton(ctx,3,"Help");
   	}
    
    // on mobile, handle touch
	if (Game.isMobile){
		tTouch = function(e) {
		    var touch, x, y;
		   
		    e.preventDefault();
		    for(var i=0;i<e.targetTouches.length;i++) {
		      touch = e.targetTouches[i];
		      x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
		      y = touch.pageY / Game.canvasMultiplier - Game.canvas.offsetTop;
		      
		      // button 1 clicked
		      if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight + marginTopBottom && y < buttonAreaHeight*2 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.clearBoard();
		      	gameMode = "level";
		      	Game.setBoard(1,new HomeScreen(playGame));
		      	Game.setBoard(2,new TouchControls());
		      }
		      // button 2 clicked
		      else if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.clearBoard();
		      	gameMode = "time";
		      	Game.setBoard(1,new HomeScreen(playGame));
		      	Game.setBoard(2,new TouchControls());
		      }
		      // button 3 clicked
		      else if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.clearBoard();
		      	Game.setBoard(1, new HelpMenu());
		      }
		    }
		  };
			  
	  	Game.canvas.addEventListener('touchstart', tTouch, false);
  }
  // on desktop, handle mouse click 
  else {
	  	tClick = function(e) {
	  		e.preventDefault();
	  		var x = e.clientX;
	  		var y = e.clientY;
	  		if (y > buttonAreaHeight + marginTopBottom && y < buttonAreaHeight*2 - marginTopBottom){
	  			Game.canvas.removeEventListener('click', tClick, false);
	  			Game.clearBoard();
	  			gameMode = "level";
		  		Game.setBoard(1,new HomeScreen(playGame));
		  	} else if (y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom) {
		  		Game.canvas.removeEventListener('click', tClick, false);
		      	Game.clearBoard();
		      	gameMode = "time";
		      	Game.setBoard(1,new HomeScreen(playGame));
		  	} else if ( y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom){
		  		Game.canvas.removeEventListener('click', tClick, false);
		  		Game.clearBoard();
		  		Game.setBoard(1, new HelpMenu());
		  	}
	  	}
	  	Game.canvas.addEventListener('click', tClick, false);
	}
  	
}
 
/**
 *	draw a menu and handle input when in level mode
 * 
 * @param {Object} levelCleared - true or false if level is successfully cleared or not
 */ 
var LevelMenu = function(levelCleared){
	var buttonAreaHeight = Game.height/noOfButtons;
	
    this.step = function(dt) { }
	
	this.draw = function(ctx) {
		
		
		if (levelCleared){			
			drawMenuLabel(ctx,0,"Level "+currentLevelIndex);
			drawMenuLabel(ctx,1,"CLEARED");
			drawMenuButton(ctx,2,"Next level");
		} else {
			drawMenuLabel(ctx,0,"Level "+(currentLevelIndex+1));
			drawMenuLabel(ctx,1,"FAILED");			
			drawMenuButton(ctx,2,"Retry level");
		}
    	drawMenuButton(ctx,3,"Main menu");
   	}
    
	if (Game.isMobile){
		tTouch = function(e) {
		    var touch, x, y;
		
		    e.preventDefault();
		    for(var i=0;i<e.targetTouches.length;i++) {
		      touch = e.targetTouches[i];
		      x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
		      y = touch.pageY / Game.canvasMultiplier - Game.canvas.offsetTop;
		      
		      // button 2 clicked
		      if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.clearBoard();
				Game.setBoard(1,new HomeScreen(playGame));
		      	Game.setBoard(2,new TouchControls());
		      }
		      // button 3 clicked
		      else if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.setBoard(1,new MainMenu());
		      }
		      
		    }
		  };
			  
	  	Game.canvas.addEventListener('touchstart', tTouch, false);
  } else {
	  	
	  	tClick = function(e) {
	  		e.preventDefault();
	  		var x = e.clientX;
	  		var y = e.clientY;
	  		
	  		// button 2 clicked
	  		if (y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom){
	  			Game.canvas.removeEventListener('click', tClick, false);
	  			Game.clearBoard();
		  		Game.setBoard(1,new HomeScreen(playGame));
		  	} 
		  	// button 3 clicked
		  	else if(y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom) {
		  		Game.canvas.removeEventListener('click', tClick, false);
		  		Game.setBoard(1,new MainMenu());
		  	}	
	  	}
	  	Game.canvas.addEventListener('click', tClick, false);
	}
}

/**
 *	draw a menu with option to retry Time Attack 
 */
var TimeAttackMenu = function(){
	var buttonAreaHeight = Game.height/noOfButtons;
	
    this.step = function(dt) { }
	
	this.draw = function(ctx) {
		
		drawMenuLabel(ctx,0,"Time attack");
		drawMenuLabel(ctx,1,"HITS: "+hits);
		drawMenuButton(ctx,2,"Try again");
		drawMenuButton(ctx,3,"Main menu");
   	}
    
	if (Game.isMobile){
		tTouch = function(e) {
		    var touch, x, y;
		
		    e.preventDefault();
		    for(var i=0;i<e.targetTouches.length;i++) {
		      touch = e.targetTouches[i];
		      x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
		      y = touch.pageY / Game.canvasMultiplier - Game.canvas.offsetTop;
		      
		      // button 2 clicked
		      if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.clearBoard();
		      	hits = 0;
				Game.setBoard(1,new HomeScreen(playGame));
		      	Game.setBoard(2,new TouchControls());
		      }
		      // button 3 clicked
		      else if (x > marginLeftRight && x < Game.width-marginLeftRight 
		      		&& y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom){
		      	Game.canvas.removeEventListener('touchstart', tTouch, false);
		      	Game.setBoard(1,new MainMenu());
		      }
		      
		    }
		  };
			  
	  	Game.canvas.addEventListener('touchstart', tTouch, false);
  } else {
	  	
	  	tClick = function(e) {
	  		e.preventDefault();
	  		var x = e.clientX;
	  		var y = e.clientY;
	  		
	  		// button 2 clicked
	  		if (y > buttonAreaHeight*2 + marginTopBottom && y < buttonAreaHeight*3 - marginTopBottom){
	  			Game.canvas.removeEventListener('click', tClick, false);
	  			Game.clearBoard();
		  		Game.setBoard(1,new HomeScreen(playGame));
		  	} 
		  	// button 3 clicked
		  	else if ( y > buttonAreaHeight*3 + marginTopBottom && y < buttonAreaHeight*4 - marginTopBottom) {
		  		Game.canvas.removeEventListener('click', tClick, false);
		  		Game.setBoard(1,new MainMenu());
		  	}	
	  	}
	  	Game.canvas.addEventListener('click', tClick, false);
	}
}

/**
 * 
 * @param {Object} sX - x coordinate of the ball
 * @param {Object} sY - y coordinate of the ball
 * @param {Object} ballType - type of ball
 * @param {Object} xSpeed - horizontal speed
 * @param {Object} ySpeed - vertical speed
 */
var BackgroundBalls = function(sX,sY,ballType,xSpeed,ySpeed){
	this.ballType = ballType;
	this.x = sX;
	this.y = sY;
	this.predznakX = 1;
   	this.predznakY = 1;
   	this.w =  SpriteSheet.map[ballType].w+5;
	
    this.step = function(dt) { 
    		this.x += xSpeed*this.predznakX;
			this.y += ySpeed*this.predznakY;
			if (this.x > Game.width-this.w || this.x <= 0){
				this.predznakX *= -1;
			};
			if (this.y > Game.height-this.w ||this.y <= 0){
				this.predznakY *= -1;
			};
    	}
	
	this.draw = function(ctx) {
		SpriteSheet.draw(ctx,this.ballType,this.x,this.y);
		}
}

/**
 *	Draw some balls in the background of the menu 
 */
var DrawBackgroundBalls = function(){
	var board = new GameBoard();
	var w = Game.width-60;
	var h = Game.height-60;
	
	var balls = ["ballXL", "ballL", "ballM", "ballM", "ballS", "ballS", "ballXS", "ballXS", "ballXS"];
	
	for(var i=0,len = balls.length;i<len;i++) {
		board.add(new BackgroundBalls(Math.random()*w,Math.random()*h,balls[i],Math.random()*3+0.1,Math.random()*3+0.1));
	}
	Game.setBoard(0, board);
}

var HelpMenu = function(){
	var noOfButtons = 10;
	var buttonHeight = Game.height/noOfButtons;
	var text = ["Fire one missile at once.",
				"Fire two missiles at once.", 
				"Fire three missiles at once.",  
				"High gravity effect on balls.",
				"Low gravity effect on balls.",
				"Slow fire missile interval.",
				"Medium fire missile interval.",
				"Fast fire missile interval."];
	var ballIcons = ["missile1",
					 "missile2",
					 "missile3",
					 "gravityH",
					 "gravityL",
					 "missileR1",
					 "missileR2",
					 "missileR3"]
	
	this.step = function(dt) {}
	
	this.draw = function(ctx) {
		
		ctx.save();
		
		ctx.font = "bold 30px arial";
		ctx.textAlign = "center";
		ctx.fillStyle =  "#CCC";
		ctx.fillText("Help", Game.width/2, buttonHeight/2+10);
		
		//draw all special balls with explanation
		for(var i=0,len = text.length;i<len;i++) {
			ctx.font = "bold 17px arial";
			ctx.textAlign = "left";
			ctx.textBaseline = 'top';
		    ctx.fillText(text[i], 70, buttonHeight*(i+1)+buttonHeight/4);
		    SpriteSheet.draw(ctx,ballIcons[i],20,buttonHeight*(i+1)+buttonHeight/4,0);
		    		    
		}
		
		// button rectange
		ctx.globalAlpha = 0.9;
		ctx.fillStyle =  "#555";    
		ctx.fillRect(Game.width/4,buttonHeight*(noOfButtons-1),Game.width/2,buttonHeight-5);
		ctx.fillStyle =  "#333";  
		ctx.fillRect(Game.width/4+5,buttonHeight*(noOfButtons-1)+5,Game.width/2-10,buttonHeight-15);
		// button text
		ctx.textAlign = "center";
		ctx.textBaseline = 'middle';
		ctx.globalAlpha = 1.0;
	    ctx.font = "bold 30px arial";
	    ctx.fillStyle =  "#CCC";
	    ctx.fillText("Back", Game.width/2, buttonHeight*(noOfButtons-1)+buttonHeight/2);
	
		ctx.restore();
	}
	
	if (Game.isMobile){
		tTouch = function(e) {
		    var touch, x, y;
		
		    e.preventDefault();
		    for(var i=0;i<e.targetTouches.length;i++) {
		      touch = e.targetTouches[i];
		      x = touch.pageX / Game.canvasMultiplier - Game.canvas.offsetLeft;
		      y = touch.pageY / Game.canvasMultiplier - Game.canvas.offsetTop;
		      
		      // Back button clicked
		      if (x > Game.width/4 && x < 3*Game.width/4 
		      		&& y > buttonHeight*(noOfButtons-1) && y < buttonHeight*(noOfButtons)){
		      		
		      		Game.canvas.removeEventListener('touchstart', tTouch, false);
		      		DrawBackgroundBalls();
		      		Game.setBoard(1,new MainMenu());
		      }    
		    }
		  };
			  
	  	Game.canvas.addEventListener('touchstart', tTouch, false);
  } else {
	  	
	  	tClick = function(e) {
	  		e.preventDefault();
	  		var x = e.clientX;
	  		var y = e.clientY;
	  		
	  		// Back button clicked
	  		if (y > buttonHeight*(noOfButtons-1) && y < buttonHeight*(noOfButtons)){
	  			Game.canvas.removeEventListener('click', tClick, false);
	  			DrawBackgroundBalls();
		  		Game.setBoard(1,new MainMenu());
		  	} 
	  	}
	  	Game.canvas.addEventListener('click', tClick, false);
	}
}

