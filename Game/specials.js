/**
 *	Create special ball
 *  
 * @param {Object} sX - x coordinate
 * @param {Object} sY - y coordinate
 * @param {Object} specialsType - type of special ball
 */
var Specials = function(sX,sY,specialsType) { 
   this.x = sX;
   this.y = sY;
   this.w =  SpriteSheet.map[specialsType].w;
   this.h =  SpriteSheet.map[specialsType].h;
   this.specialsType = specialsType;
}

   Specials.prototype.step = function(dt) {
		this.y += 1.5; 
		if (this.y > Game.height-this.h){
			this.board.remove(this);	
		}
					
   }

   Specials.prototype.draw = function(ctx) {
     SpriteSheet.draw(ctx,this.specialsType,this.x,this.y);
   }
   
   Specials.prototype.type = OBJECT_SPECIALS;
   
/**
 *	Handle game behaviour due to special ball type
 *  
 * @param {Object} ship - ship parameters has to be set on this object
 * @param {Object} special - special ball object that was hit
 */
var handleSpecials = function(ship, special){
		if (special.specialsType == "star"){
			// not used yet
		} else if (special.specialsType == "missile1"){
			noOfMissiles = 1;
		} else if (special.specialsType == "missile2"){
			noOfMissiles = 2;
		} else if (special.specialsType == "missile3"){
			noOfMissiles = 3;
		} else if (special.specialsType == "gravityH"){
			ballGravity = 0.02;
		} else if (special.specialsType == "gravityL"){
			ballGravity = 0.001;
		} else if (special.specialsType == "missileR1"){
			ship.reloadTime = 0.6;
		} else if (special.specialsType == "missileR2"){
			ship.reloadTime = 0.4;
		} else if (special.specialsType == "missileR3"){
			ship.reloadTime = 0.2;
		} 
}
